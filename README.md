A makefile for lifting the gnomAD (2.0.2) vcf file over to GRCh38 coordinates

I am not affilliated with the [gnomAD](http://gnomad.broadinstitute.org) project
(or else I probably would just map the reads themselves against GRCh38 and go
through a proper calling pipeline).

I also am not convinced that this has entirely correct output yet (in particular,
I know that Picard's LiftoverVcf does not handle reversing tags in multi-allelic
variants when the reference sequence allele changes). So, use at your own risk... 

The code is intended to work with data from the gnomAD consortium, which is available
under the [ODBL 1.0](https://opendatacommons.org/files/2018/02/odbl-10.txt) license.
