# Maybe someday the gnomAD folks will produce a version in GRCh38 (now ~4.5 years old as of this
# writing), and make this obsolete...

# NOTE: I use RefSeq identifiers for sequence names, so there is some awk weirdness (awk-wardness?) below...

# REQUIREMENTS:
# * curl (to download stuff, would be straightforward to use wget instead...)
# * bcftools (used to remove some info tags to make the output 'slimmer', also indexing)
# * picard (I have a simple wrapper called PicardCommandLine)

# TODO:
# * LiftoverVcf does not handle changes of the reference when the site is multiallelic.
# * Delete this when gnomAD is released in GRCh38...

# I use some bashisms (process redirection)
SHELL := /bin/bash

ifdef USE_MODULES
LOAD_BCFTOOLS := module load bcftools/1.8;
LOAD_SAMTOOLS := module load samtools/1.8;
LOAD_PICARD := module load PicardCommandLine/2.19.0;
else
LOAD_BCFTOOLS :=
LOAD_SAMTOOLS :=
LOAD_PICARD :=
endif

GNOMAD_RELEASE_BASE := https://storage.googleapis.com/gnomad-public/release
GNOMAD_VERSION := 2.1.1

# Download the gnomad exome release file
GNOMAD_SITES_FILE:=gnomad.exomes.r$(GNOMAD_VERSION).sites.vcf.bgz
GNOMAD_STEM=${GNOMAD_SITES_FILE%.vcf.bgz}

.PHONY: all

.SECONDARY:

all: gnomad.exomes.r$(GNOMAD_VERSION).sites.slim.refseq38.norm.vcf.gz gnomad.exomes.r$(GNOMAD_VERSION).sites.slim.refseq38.norm.vcf.gz.tbi

$(GNOMAD_SITES_FILE):
	curl -L $(GNOMAD_RELEASE_BASE)/$(GNOMAD_VERSION)/vcf/exomes/$(GNOMAD_SITES_FILE) -o $(GNOMAD_SITES_FILE)

$(GNOMAD_SITES_FILE).tbi:
	curl -L $(GNOMAD_RELEASE_BASE)/$(GNOMAD_VERSION)/vcf/exomes/$(GNOMAD_SITES_FILE).tbi -o $(GNOMAD_SITES_FILE).tbi

%.slim.vcf.gz : %.vcf.bgz %.vcf.bgz.tbi
	@echo "Focusing on a smaller number of tags, decomposing"
	$(LOAD_BCFTOOLS) \
	    bcftools annotate \
	      -x '^INFO/AF,INFO/AF_popmax,INFO/AF_female,INFO/AF_male,INFO/AC,INFO/AC_popmax,INFO/AC_female,INFO/AC_male,INFO/AN,INFO/AN_popmax,INFO/AN_female,INFO/AN_male,INFO/nhomalt,INFO/nhomalt_popmax,INFO/popmax' \
	      -Ou "$<" \
	    | bcftools norm -N -m - -Oz -o "$@" -

# Picard's LiftOverVcf needs a reference genome and a sequence dictionary, so let's fetch the former
#  and make the latter
GCF_000001405.26_GRCh38_genomic.fna:
	curl https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/001/405/GCF_000001405.26_GRCh38/GCF_000001405.26_GRCh38_genomic.fna.gz \
	    | gzip -dc > "$@"

%.fna.dict : %.fna
	$(LOAD_PICARD) \
	PicardCommandLine CreateSequenceDictionary AS=GRCh38 R="$<" O="$@"

%.vcf.gz.tbi : %.vcf.gz
	$(LOAD_BCFTOOLS) \
	bcftools index -t "$<"

GRCh37_to_GRCh38.chain.gz:
	curl -L "https://sourceforge.net/projects/crossmap/files/Ensembl_chain_files/homo_sapiens%28human%29/GRCh37_to_GRCh38.chain.gz/download" \
	    -o "$@"

# get ready for the weirdest awk code you will probably see today...
#   the 'NR==FNR' trick lets me do something different for the first file (the assembly report, where the
#   desired mapping of names are read), from the second file (the chain file).
# sed is used to change spaces to tabs (to be consistent with the other file being processed in awk)
#   and then back to spaces as is typical for chain files
GCA_FTP_DIR='ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/001/405/GCA_000001405.15_GRCh38/'
# if using a chain file from UCSC, will need to use $10 instead of $1 in the assembly_report

%.refseq.chain.gz : %.chain.gz
	awk -F$$'\t' '/^#/ { next } NR==FNR { gsub(/CHR_/, "", $$1); acc[$$1]=$$7; next } /^chain/ { $$8 = acc[$$8]?acc[$$8]:$$8; print $$0; next } 1' \
	    <(curl $(GCA_FTP_DIR)/GCA_000001405.15_GRCh38_assembly_report.txt | sed -e 's/\r//g') \
	    <(zcat "$<" | sed -e 's/ /\t/g') \
	    | sed -e 's/\t/ /g' | gzip > "$@"

#TAGS_TO_REVERSE := AF_AFR,AF_AMR,AF_ASJ,AF_EAS,AF_FIN,AF_NFE,AF_OTH,AF_SAS,AF_Male,AF_Female,AF_raw,AF_POPMAX,AF_AMR_Male,AF_FIN_Female,AF_FIN_Male,AF_AFR_Male,AF_SAS_Male,AF_OTH_Male,AF_NFE_Female,AF_EAS_Female,AF_EAS_Male,AF_SAS_Female,AF_AFR_Female,AF_AMR_Female,AF_ASJ_Male,AF_ASJ_Female,AF_OTH_Female,AF_NFE_Male
TAGS_TO_REVERSE := AF,AF_popmax,AF_female,AF_male

%.refseq38.vcf.gz: %.vcf.gz GCF_000001405.26_GRCh38_genomic.fna.dict GRCh37_to_GRCh38.refseq.chain.gz
	$(LOAD_PICARD) \
	  export JAVA_OPTIONS=-Xmx24g; \
	  PicardCommandLine LiftoverVcf \
	    INPUT="$<" \
	    OUTPUT="$@" \
	    REFERENCE_SEQUENCE=GCF_000001405.26_GRCh38_genomic.fna \
	    CHAIN=GRCh37_to_GRCh38.refseq.chain.gz \
	    REJECT="$$(basename $@ .vcf.gz).reject.vcf.gz" \
	    TAGS_TO_REVERSE=$(TAGS_TO_REVERSE) \
	    MAX_RECORDS_IN_RAM=500000

%.fna.fai : %.fna
	$(LOAD_SAMTOOLS) \
	  samtools faidx "$<"

%.refseq38.norm.vcf.gz : %.refseq38.vcf.gz GCF_000001405.26_GRCh38_genomic.fna.fai
	$(LOAD_BCFTOOLS) \
	    bcftools norm -f GCF_000001405.26_GRCh38_genomic.fna -Oz -o "$@" "$<"
